﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace DniproFuture.Models
{
    public class PartnersOutputModel
    {
        public string Title { get; set; }
        public string Logo { get; set; }
    }
}